﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="do-work.aspx.cs" Inherits="do_work" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Qorking now</title>
    <script src="Scripts/jquery-2.1.1.js"></script>
    <script src="Scripts/TumblrScripts.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var info = getUserFromInfoResponse(<% =this.info %>);
            var json = <%=this.json %>
            var requestType = <%=this.requestType%>
            $('#lblTest').text(info.blogurl);
            var str = "";
            for (var key in json.response.posts) {
                if (json.response.posts.hasOwnProperty(key)) {
                    str += (json.response.posts[key].blog_name+", ");
                }
            }
            $('#lblStatus').text(str);
            sessionStorage.setItem("responseData", json);

        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblTest" runat="server" />
        <br />
        <label>Parameter 1 Key</label>
        <br />
        <asp:TextBox ID="txtParamKey1" runat="server"></asp:TextBox>
        <br />
        <label>Parameter 1 Content</label>
        <br />
        <asp:TextBox ID="txtParamContent1" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="btnPost" Text="Post" runat="server" OnClick="btnPost_Click" /><asp:Label ID="lblStatus" runat="server" /><asp:Label ID="lblSuccess" runat="server" />
        <br />
        <br />
        <asp:Button ID="btnBlogInfo" Text="Blog Info" runat="server" OnClick="btnBlogInfo_Click"/><asp:Label ID="lblBlogInfoResult" runat="server" /><br />
        <asp:Button ID="btnBlogAvatar" Text="Blog Avatar" runat="server" OnClick="btnBlogAvatar_Click"/><asp:Label ID="lblBlogAvatarResult" runat="server" /><br />
        <asp:Button ID="btnBlogFollowers" Text="Blog Followers" runat="server" OnClick="btnBlogFollowers_Click"/><asp:Label ID="lblBlogFollowersResult" runat="server" /><br />
        <asp:Button ID="btnBlogLikes" Text="Blog Likes" runat="server" OnClick="btnBlogLikes_Click"/><asp:Label ID="lblBlogLikesResult" runat="server" /><br />
        <asp:Button ID="btnBlogPosts" Text="Blog Posts" runat="server" OnClick="btnBlogPosts_Click"/><asp:Label ID="lblBlogPostsResult" runat="server" /><br />
        <asp:Button ID="btnBlogPostsQueue" Text="Blog Queued Posts" runat="server" OnClick="btnBlogPostsQueue_Click"/><asp:Label ID="lblBlogPostsQueueResult" runat="server" /><br />
        <asp:Button ID="btnBlogPostsDrafts" Text="Blog Draft Posts" runat="server" OnClick="btnBlogPostsDrafts_Click"/><asp:Label ID="lblBlogPostsDraftsResult" runat="server" /><br />
        <asp:Button ID="btnBlogPostsSubmissions" Text="Blog Post Submissions" runat="server" OnClick="btnBlogPostsSubmissions_Click"/><asp:Label ID="lblBlogPostsSubmissionsResult" runat="server" /><br />
        <asp:Button ID="btnBlogPost" Text="Blog Post" runat="server" OnClick="btnBlogPost_Click"/><asp:Label ID="lblBlogPostResult" runat="server" /><br />
        <asp:Button ID="btnBlogPostEdit" Text="Blog Edit Post" runat="server" OnClick="btnBlogPostEdit_Click"/><asp:Label ID="lblBlogPostEditResult" runat="server" /><br />
        <asp:Button ID="btnBlogPostReblog" Text="Blog Reblog Post" runat="server" OnClick="btnBlogPostReblog_Click"/><asp:Label ID="lblBlogPostReblogResult" runat="server" /><br />
        <asp:Button ID="btnBlogPostDelete" Text="Blog Delete Post" runat="server" OnClick="btnBlogPostDelete_Click"/><asp:Label ID="lblBlogPostDeleteResult" runat="server" /><br />
        <asp:Button ID="btnUserInfo" Text="User Info" runat="server" OnClick="btnUserInfo_Click"/><asp:Label ID="lblUserInfoResult" runat="server" /><br />
        <asp:Button ID="btnUserDashboard" Text="User Dashboard" runat="server" OnClick="btnUserDashboard_Click"/><asp:Label ID="lblUserDashboardResult" runat="server" /><br />
        <asp:Button ID="btnUserLikes" Text="User Likes" runat="server" OnClick="btnUserLikes_Click"/><asp:Label ID="lblUserLikesResult" runat="server" /><br />
        <asp:Button ID="btnUserFollowing" Text="User Following" runat="server" OnClick="btnUserFollowing_Click"/><asp:Label ID="lblUserFollowingResult" runat="server" /><br />
        <asp:Button ID="btnUserFollow" Text="User Follow" runat="server" OnClick="btnUserFollow_Click"/><asp:Label ID="lblUserFollowResult" runat="server" /><br />
        <asp:Button ID="btnUserUnfollow" Text="User Unfollow" runat="server" OnClick="btnUserUnfollow_Click"/><asp:Label ID="lblUserUnFollowResult" runat="server" /><br />
        <asp:Button ID="btnUserLike" Text="User Like" runat="server" OnClick="btnUserLike_Click"/><asp:Label ID="lblUserLikeResult" runat="server" /><br />
        <asp:Button ID="btnUserUnlike" Text="User Unlike" runat="server" OnClick="btnUserUnlike_Click"/><asp:Label ID="lblUserUnlikeResult" runat="server" /><br />
        <asp:Button ID="btnTagged" Text="Tagged" runat="server" OnClick="btnTagged_Click"/><asp:Label ID="lblTaggedResult" runat="server" /><br />
    </div>
    </form>
</body>
</html>
