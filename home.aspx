﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="home.aspx.cs" Inherits="home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function openLogInWindow() {
            var url = "<%=this.authorizationlink%>"
            window.open(url, '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no');
        }
    </script>
</head>
<body>
     
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>   
    <div>
        <asp:Label Text="" ID="lblReturn" runat="server" />
        <asp:Button Text="Authorize" ID="btnReqTkn" runat="server" OnClientClick="openLogInWindow();" OnClick="btnReqTkn_Click" />
    </div>
    <asp:HiddenField runat="server" ID="hdnData"/>
        <br />
    <asp:Button id="btnUpdate" style="display:none" runat="server" OnClick="btnUpdate_Click" />

    </form>
</body>
</html>