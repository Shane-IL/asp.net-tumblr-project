﻿using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DevDefined.OAuth.Utility;

public partial class do_work : System.Web.UI.Page
{
    private OAuthSession session;
    private IToken accessTkn;
    public string info;
    public string json;
    public string requestType;
    private string blogurl;
    private TumblrCode tc = new TumblrCode();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["accessToken"] != null)
        {
            session = (OAuthSession)Session["sessionData"];
            accessTkn =(IToken)Session["accessToken"];
            session.AccessToken = accessTkn;
            info = session.Request().Get().ForUrl("http://api.tumblr.com/v2/user/info?type=text").ToString();
            blogurl = tc.getBlogUrl(info);
            
           
        }
    }
    protected void btnPost_Click(object sender, EventArgs e)
    {
        
    }

    void ShowStatusCodeDetails(HttpWebResponse response)
    {
        

        NameValueCollection parameters = HttpUtility.ParseQueryString(response.ReadToEnd());

        string str2 = string.Format("Request failed, status code was: {0}, status description: {1}", response.StatusCode, response.StatusDescription);
    }

    void ShowOAuthProblemDetails(WebResponse response)
    {
       

        NameValueCollection parameters = HttpUtility.ParseQueryString(response.ReadToEnd());

        string str3 = "Access was denied to resource.<br/><br/>";

        foreach (string key in parameters.Keys)
            str3 += key + " => " + parameters[key] + "<br/>";
        lblSuccess.Text = str3;
    }
    protected void btnBlogInfo_Click(object sender, EventArgs e)
    {
        string response = null;

        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add("api_key", TumblrCode.consumerKey);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogInfo, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogInfoResult.Text = response;
            json = response;
            requestType = "blog-info";
        }

        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogAvatar_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add("size", txtParamKey1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogAvatar, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogAvatarResult.Text = response;
            json = response;
            requestType = "blog-avatar";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogFollowers_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogFollowers, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogFollowersResult.Text = response;
            json = response;
            requestType = "blog-followers";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogLikes_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add("api_key", TumblrCode.consumerKey);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogLikes, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogLikesResult.Text = response;
            json = response;
            requestType = "blog-likes";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogPosts_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add("api_key", TumblrCode.consumerKey);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogPosts, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogPostsResult.Text = response;
            json = response;
            requestType = "blog-posts";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogPostsQueue_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogPostsQueue, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogPostsQueueResult.Text = response;
            json = response;
            requestType = "blog-posts-queue";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogPostsDrafts_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogPostsDrafts, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogPostsDraftsResult.Text = response;
            json = response;
            requestType = "blog-posts-drafts";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogPostsSubmissions_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogPostsSubmissions, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogPostsSubmissionsResult.Text = response;
            json = response;
            requestType = "blog-posts-submissions";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogPost_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogPost, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogPostResult.Text = response;
            json = response;
            requestType = "blog-post";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogPostEdit_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogPostEdit, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogPostEditResult.Text = response;
            json = response;
            requestType = "blog-post-edit";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogPostReblog_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogPostReblog, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogPostReblogResult.Text = response;
            json = response;
            requestType = "blog-post-reblog";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnBlogPostDelete_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.BlogPostDelete, blogurl);
            response = tc.getResponse(instructions, details, session);
            lblBlogPostDeleteResult.Text = response;
            json = response;
            requestType = "blog-post-delete";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnUserInfo_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.UserInfo);
            response = tc.getResponse(instructions, details, session);
            lblUserInfoResult.Text = response;
            json = response;
            requestType = "user-info";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnUserDashboard_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.UserDashboard);
            response = tc.getResponse(instructions, details, session);
            lblUserDashboardResult.Text = response;
            json = response;
            requestType = "user-dashboard";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnUserLikes_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.UserLikes);
            response = tc.getResponse(instructions, details, session);
            lblUserLikesResult.Text = response;
            json = response;
            requestType = "user-likes";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnUserFollowing_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.UserFollowing);
            response = tc.getResponse(instructions, details, session);
            lblUserFollowingResult.Text = response;
            json = response;
            requestType = "user-following";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnTagged_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add("api_key", TumblrCode.consumerKey);
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.Tagged);
            response = tc.getResponse(instructions, details, session);
            lblTaggedResult.Text = response;
            json = response;
            requestType = "tagged";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnUserFollow_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.UserFollow);
            response = tc.getResponse(instructions, details, session);
            lblUserFollowResult.Text = response;
            json = response;
            requestType = "user-follow";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnUserUnfollow_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.UserUnfollow);
            response = tc.getResponse(instructions, details, session);
            lblUserUnFollowResult.Text = response;
            json = response;
            requestType = "user-unfollow";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnUserLike_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.UserLike);
            response = tc.getResponse(instructions, details, session);
            lblUserLikeResult.Text = response;
            json = response;
            requestType = "user-like";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
    protected void btnUserUnlike_Click(object sender, EventArgs e)
    {
        string response = null;
        try
        {
            Dictionary<string, string> details = new Dictionary<string, string>();
            details.Add(txtParamKey1.Text, txtParamContent1.Text);
            Dictionary<string, string> instructions = tc.requestUrlBuilder(TumblrCode.requestType.UserUnlike);
            response = tc.getResponse(instructions, details, session);
            lblUserUnlikeResult.Text = response;
            json = response;
            requestType = "user-unlike";
        }


        catch (WebException webEx)
        {
            var res = (HttpWebResponse)webEx.Response;
            if (res.StatusCode == HttpStatusCode.Forbidden)
            {
                ShowOAuthProblemDetails(res);
            }
            else
            {
                ShowStatusCodeDetails(res);
            }

        }
    }
}