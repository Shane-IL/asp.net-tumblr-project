﻿function getUserFromInfoResponse(answer) {
    var username = answer.response.user.name;
    var blogurl = answer.response.user.blogs[0].url;
    blogurl = blogurl.replace("http:", "").replace("//", "").replace("/", "").trim();
    var userdetails ={name: username, blogurl: blogurl};
    return userdetails;
}



function dataCatcher() {
    var data = {};
    if (sessionStorage.getItem("responseData") != null) { data.json = sessionStorage.getItem("responseData") }
    if (sessionStorage.getItem("requestType")!=null){data.requestType = sessionStorage.getItem("requestType")}
    return data;
};



