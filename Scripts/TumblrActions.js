﻿var tumblrActions = {
    requests: {
        blogMethods: {
            info: {suffix: "/blog/{blogurl}/info", method: "GET", authentication: "apiKey", requiredParameters: ["api_key"], optionalParameters: []},
            avatar: {suffix: "/blog/{blogurl}/avatar", method: "GET", authentication: "none", requiredParameters: [], optionalParameters: ["size"]},
            likes: {suffix: "/blog/{blogurl}/likes", method: "GET", authentication: "apiKey", requiredParameters: ["api_key"], optionalParameters: ["limit", "offset"]},
            followers: {suffix: "/blog/{blogurl}/followers", method: "GET", authentication: "OAuth", requiredParameters: [], optionalParameters: ["limit", "offset"]},
            posts: {suffix: "/blog/{blogurl}/posts/{type}", method: "GET", authentication: "apiKey", requiredParameters: ["api_key"], optionalParameters: ["type", "id", "tag", "limit", "offset", "reblog_info", "notes_info", "filter"]},
            queuedPosts: {suffix: "/blog/{blogurl}/posts/queue", method: "GET", authentication: "OAuth", requiredParameters: [], optionalParameters: ["offset", "limit", "filter"]},
            draftPosts: {suffix: "/blog/{blogurl}/posts/draft", method: "GET", authentication: "OAuth", requiredParameters: [], optionalParameters: ["before_id", "filter"]},
            submissionPosts: {suffix: "/blog/{blogurl}/posts/submission", method: "GET", authentication: "OAuth", requiredParameters: [], optionalParameters: ["offset", "filter"]},
            createNewTextPost: {suffix: "/blog/{blogurl}/post", method: "POST", authentication: "OAuth", requiredParameters: ["type", "body"], optionalParameters: ["title", "state", "tags", "tweet", "date", "format", "slug"]},
            createNewPhotoPost: {suffix: "/blog/{blogurl}/post", method: "POST", authentication: "OAuth", requiredParameters: ["type", "source", "data"], optionalParameters: ["caption", "link", "state", "tags", "tweet", "date", "format", "slug"]},
            createNewQuotePost: {suffix: "/blog/{blogurl}/post", method: "POST", authentication: "OAuth", requiredParameters: ["type", "quote"], optionalParameters: ["source", "state", "tags", "tweet", "date", "format", "slug"]},
            createNewLinkPost: {suffix: "/blog/{blogurl}/post", method: "POST", authentication: "OAuth", requiredParameters: ["type", "url"], optionalParameters: ["title", "description", "state", "tags", "tweet", "date", "format", "slug"]},
            createNewChatPost: {suffix: "/blog/{blogurl}/post", method: "POST", authentication: "OAuth", requiredParameters: ["type", "conversation"], optionalParameters: ["title", "state", "tags", "tweet", "date", "format", "slug"]},
            createNewAudioPost: {suffix: "/blog/{blogurl}/post", method: "POST", authentication: "OAuth", requiredParameters: ["type", "external_url", "data"], optionalParameters: ["caption", "state", "tags", "tweet", "date", "format", "slug"]},
            createNewVideoPost: {suffix: "/blog/{blogurl}/post", method: "POST", authentication: "OAuth", requiredParameters: ["type", "embed", "data"], optionalParameters: ["caption", "state", "tags", "tweet", "date", "format", "slug"]},
            reblogPost: {suffix: "/blog/{blogurl}/post/reblog", method: "POST", authentication: "OAuth", requiredParameters: ["id", "reblog_key", "comment"], optionalParameters: []},
            deletePost: {suffix: "/blog/{blogurl}/post/delete", method: "POST", authentication: "OAuth", requiredParameters: ["id"], optionalParameters: []}
        },

        userMethods: {
            getUserInfo: {suffix: "/user/info", method: "GET", authentication: "OAuth", requiredParameters: [], optionalParameters: []},
            getUserDashboard: {suffix: "/user/dashboard", method: "GET", authentication: "OAuth", requiredParameters: [], optionalParameters: ["limit", "offset", "type", "since_id", "reblog_info", "notes_info"]},
            getUserLikes: {suffix: "/user/likes", method: "GET", authentication: "OAuth", requiredParameters: [], optionalParameters: ["limit", "offset"]},
            getUserFollowing: {suffix: "/user/following", method: "GET", authentication: "OAuth", requiredParameters: [], optionalParameters: ["limit", "offset"]},
            followBlog: {suffix: "/user/follow", method: "POST", authentication: "OAuth", requiredParameters: ["url"], optionalParameters: []},
            unFollowBlog: {suffix: "/user/unfollow", method: "POST", authentication: "OAuth", requiredParameters: ["url"], optionalParameters: []},
            likePost: {suffix: "/user/follow", method: "POST", authentication: "OAuth", requiredParameters: ["id", "reblog_key"], optionalParameters: []},
            unLikePost: {suffix: "/user/unlike", method: "POST", authentication: "OAuth", requiredParameters: ["id", "reblog_key"], optionalParameters: []}
        },

        taggedMethod: {
            getTaggedPosts: {suffix: "/tagged", method: "GET", authentication: "apiKey", requiredParameters: ["tag", "api_key"], optionalParameters: ["before", "limit", "filter"]}
        }
    }
};


