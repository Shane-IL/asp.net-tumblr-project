﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevDefined.OAuth;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;

public partial class home : System.Web.UI.Page
{
    public string authorizationlink;
    private OAuthSession session;
    private IToken reqTkn;
    public OAuthSession sessionData { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            OAuthManager om = new OAuthManager();
            session = om.createSession();
            reqTkn = session.GetRequestToken();
            Session["sessionData"] = session;
            Session["reqToken"] = reqTkn;
            authorizationlink = session.GetUserAuthorizationUrlForToken(reqTkn); 

        }
        if (IsPostBack)
        {
            reqTkn = (IToken)Session["reqToken"];
            session = (OAuthSession)Session["sessionData"];

        }
    }

    
    protected void btnReqTkn_Click(object sender, EventArgs e)
    {
        
        
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        HttpCookie verify = new HttpCookie("verify");
        verify = Request.Cookies["Verifier"]; 
        string ver = verify.Value;
        IToken accessToken = session.ExchangeRequestTokenForAccessToken(reqTkn, ver);
        Session["accessToken"] = accessToken;
        Response.Redirect("do-work.aspx");
        

    }
}