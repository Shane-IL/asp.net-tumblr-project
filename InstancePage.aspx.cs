﻿using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InstancePage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string varcode = "";

        if (Request.QueryString["oauth_verifier"] != null && Request.QueryString["oauth_verifier"].Length > 0)
        {
            HttpCookie verifier = new HttpCookie("Verifier");
            varcode = Request.QueryString["oauth_verifier"];
            verifier.Value = varcode;
            verifier.Expires = DateTime.Now.AddSeconds(10);
            Response.Cookies.Add(verifier);
            lblTest.Text = "OK";
            
        }
        else
        {
            lblTest.Text = "No Value Detected";
        }

        

        String strscript = "<script type='text/javascript'>closeWindow()</script>";
        Page.RegisterStartupScript("clientScript", strscript);
        
    }
}