﻿using DevDefined.OAuth.Consumer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TumblrCode
/// </summary>
public class TumblrCode
{
    public enum requestType
    {
        BlogInfo,
        BlogAvatar,
        BlogFollowers,
        BlogLikes,
        BlogPosts,
        BlogPostsQueue,
        BlogPostsDrafts,
        BlogPostsSubmissions,
        BlogPost,
        BlogPostEdit,
        BlogPostReblog,
        BlogPostDelete,
        UserInfo,
        UserDashboard,
        UserLikes,
        UserFollowing,
        UserFollow,
        UserUnfollow,
        UserLike,
        UserUnlike,
        Tagged
    }
    private string apiurl = "http://api.tumblr.com/v2";
    private string blogmethodparam = "/blog/{blogurl}/";
    private string usermethodparam = "/user/";
    private static string _consumerKey = PrivateClass.cons;

    public static string consumerKey { get { return _consumerKey; } }
    public Dictionary<string,string> requestUrlBuilder(requestType type, string blogurl = "option", string slashparam = "option")
    {
        string suffix;
        Dictionary<string, string> instructions = new Dictionary<string, string>();
        switch (type)
        {
            #region Blog request methods

            case (requestType.BlogInfo):
                suffix = apiurl + blogmethodparam + "info";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "api_key");
                return instructions;

            case (requestType.BlogAvatar):
                suffix = apiurl + blogmethodparam + "avatar";
                suffix = blogUrlReplacer(blogurl, suffix);
                if (slashparam != "option") { suffix += "/" + slashparam; }
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "none");
                return instructions;

            case (requestType.BlogFollowers):
                suffix = apiurl + blogmethodparam + "followers";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.BlogLikes):
                suffix = apiurl + blogmethodparam + "likes";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "api_key");
                return instructions;

            case (requestType.BlogPosts):
                suffix = apiurl + blogmethodparam + "posts";
                suffix = blogUrlReplacer(blogurl, suffix);
                if (slashparam != "option") { suffix += "/" + slashparam; }
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "api_key");
                return instructions;

            case (requestType.BlogPostsQueue):
                suffix = apiurl + blogmethodparam + "posts/queue";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.BlogPostsDrafts):
                suffix = apiurl + blogmethodparam + "posts/draft";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.BlogPostsSubmissions):
                suffix = apiurl + blogmethodparam + "posts/submission";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.BlogPost):
                suffix = apiurl + blogmethodparam + "post";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "POST");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.BlogPostEdit):
                suffix = apiurl + blogmethodparam + "post/edit";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "POST");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.BlogPostReblog):
                suffix = apiurl + blogmethodparam + "post/reblog";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "POST");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.BlogPostDelete):
                suffix = apiurl + blogmethodparam + "post/delete";
                suffix = blogUrlReplacer(blogurl, suffix);
                instructions.Add("url", suffix);
                instructions.Add("method", "POST");
                instructions.Add("authentication", "OAuth");
                return instructions; 

            #endregion

            #region User request methods
            case (requestType.UserInfo):
                suffix = apiurl + usermethodparam + "info";
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.UserDashboard):
                suffix = apiurl + usermethodparam + "dashboard";
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.UserLikes):
                suffix = apiurl + usermethodparam + "likes";
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.UserFollowing):
                suffix = apiurl + usermethodparam + "following";
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.UserFollow):
                suffix = apiurl + usermethodparam + "follow";
                instructions.Add("url", suffix);
                instructions.Add("method", "POST");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.UserUnfollow):
                suffix = apiurl + usermethodparam + "unfollow";
                instructions.Add("url", suffix);
                instructions.Add("method", "POST");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.UserLike):
                suffix = apiurl + usermethodparam + "like";
                instructions.Add("url", suffix);
                instructions.Add("method", "POST");
                instructions.Add("authentication", "OAuth");
                return instructions;

            case (requestType.UserUnlike):
                suffix = apiurl + usermethodparam + "unlike";
                instructions.Add("url", suffix);
                instructions.Add("method", "POST");
                instructions.Add("authentication", "OAuth");
                return instructions; 
            #endregion 


            case (requestType.Tagged):
                suffix = apiurl + "/tagged";
                instructions.Add("url", suffix);
                instructions.Add("method", "GET");
                instructions.Add("authentication", "api_key");
                return instructions;

            default:
                instructions.Add("method", "ERR");
                return instructions;
        }
    }
    public void requestParamaterAdder(Dictionary<string, string> parameters, string key, string value)
    {
        parameters.Add(key, value);
    }
    public string blogUrlReplacer(string blogurl, string suffix)
    {
        int start = suffix.IndexOf("{");
        int end = suffix.IndexOf("}")-suffix.IndexOf("{")+1;
        string toReplace = suffix.Substring(start ,end );
        suffix = suffix.Replace(toReplace, blogurl);
        return suffix;
    }
    public string getBlogUrl(string json)
    {
        string blogUrl =json;
        blogUrl = blogUrl.Substring(blogUrl.IndexOf("url") + 11, blogUrl.IndexOf("followers") - blogUrl.IndexOf("url") - 11);
        blogUrl = blogUrl.Replace("http:", "").Replace("\"", "").Replace("/", "").Replace("\\", "").Trim();
        blogUrl = blogUrl.Substring(0, blogUrl.IndexOf(".com") + 4);
        return blogUrl;
    }

    public string getResponse(Dictionary<string, string> instructions, Dictionary<string,string> details, OAuthSession session)
    {
        string url = instructions["url"];
        string method = instructions["method"];
        string response ="Error";
        if (method == "GET")
        {
            response = session.Request().Get().ForUrl(url).WithQueryParameters(details).ToString();
        }
        else if (method == "POST")
        {
            response = session.Request().Post().ForUrl(url).WithFormParameters(details).ToString();
        }
        else if (method == "ERR")
        {
            //Add further error handling
            return response;
        }

        return response;
    }
}

