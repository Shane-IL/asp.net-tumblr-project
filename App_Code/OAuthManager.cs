﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevDefined.OAuth;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;

/// <summary>
/// Summary description for OAuthManager
/// </summary>
public class OAuthManager
{
	public OAuthManager()
	{
		//
		// TODO: Add constructor logic here
		//
       
	}
    
    public OAuthSession createSession(){

        string requestUrl = "http://www.tumblr.com/oauth/request_token";
        string userAuthorizeUrl = "http://www.tumblr.com/oauth/authorize";
        string accessUrl = "http://www.tumblr.com/oauth/access_token";
        string callbackUrl = "http://localhost:9618/InstancePage.aspx";

        var consumerConext = new OAuthConsumerContext 
        {
            //Consumer Key and Secret are stored in a separate class that is not published in public repo, simply replace with your own.
            ConsumerKey = PrivateClass.cons,
            ConsumerSecret = PrivateClass.sec,
            SignatureMethod = SignatureMethod.HmacSha1,
            
        
        };

        var session = new OAuthSession(consumerConext, requestUrl, userAuthorizeUrl, accessUrl, callbackUrl);
        
        return session;
        

    }

    
}